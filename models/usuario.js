/*var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
  nombre: String,
});

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
  var reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId, 
    desde: desde, 
    hasta: hasta
  });
  console.log(reserva);
  reserva.save(cb);
}

module.exports = mongoose.model('Usuario', usuarioSchema);

*/


var mongoose = require('mongoose');
var Reserva = require('./reserva');
const uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
const { updateOne } = require('./reserva');
const Token = require('./token')
const mailer = require('../mailer/mailer')
var crypto = require('crypto');


const saltRounds = 10;

const validateEmail = function(email){
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

var usuarioSchema = new Schema({
    nombre:{
        type: String,
        trim: true,
        require: [true , 'El nombre es obligatorio']
    },
    email:{
        type: String,
        trim: true,
        required: [true, 'El mail es requerido'],
        loadClass: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/]
    },
    password:{
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, {message:'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}
 
usuarioSchema.statics.reservar = function(_id, biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario:_id, bicicleta: biciId, desde:desde, hasta:hasta});
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.statics.add = function(aUsuario, cb){
    this.create(aUsuario,cb);
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(
    condition,
    callback
) {
    const self = this;
    console.log(condition);
    self.findOne(
        {
            $or: [
                { googleId: condition.id },
                { email: condition.emails[0].value },
            ],
        },
        function (err, result) {
            if (result) {
                callback(err, result);
            } else {
                let values = {};
                values.googleId = condition.id;
                values.email = condition.email[0].values;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = condition._json.etag;
                self.create(values, (err, result) => {
                    if (err) console.log(err);
                    return callback(err, result);
                });
            }
        }
    );
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(
    condition,
    callback
) {
    const self = this;
    console.log(condition);
    self.findOne(
        {
            $or: [
                { facebookId: condition.id },
                { email: condition.emails[0].value },
            ],
        },
        function (err, result) {
            if (result) {
                callback(err, result);
            } else {
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.email[0].values;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');
                self.create(values, (err, result) => {
                    if (err) console.log(err);
                    return callback(err, result);
                });
            }
        }
    );
};

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    const reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde,
        hasta,
    });
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.methods.enviarEmailBienvenida = function(cb){

    console.log('Usuario: ' + this.nombre + ' ' + this.id + ' _ ' + this._id);

    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_to = this.email;
    token.save(function(err){
        if (err) return console.log(err.message);

        const mailOptions = {
            from: 'rudy.manzaneda@gmail.com',
            to: email_to,
            subject: 'Verificación de cuenta',
            text: 'Hola! \n\n para verificar su cuenta haga click en el siguiente link \n http://localhost:5000' + '\/token/confirmation\/' + token.token + '\n'        
        };

        mailer.sendMail(mailOptions, function(err){
            if(err) return console.log(err.message);

            console.log('La verificación del mail fue enviada a ' + email_to);
        })
    });
};

usuarioSchema.methods.resetPassword = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err) {
        if (err) { return cb(err); }

        const mailOptions = {
            from: 'josh.reinger@ethereal.email',
            to: email_destination,
            subject: 'Password reset',
            text: 'Hi,\n\n' + 'Please click on this link to reset your account password:\n' + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '\n'
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) { return cb(err); }
            console.log('An email to reset the password was sent to ' + email_destination + '.');
        });

        cb(null);
    });
};


module.exports = mongoose.model('Usuario', usuarioSchema);


