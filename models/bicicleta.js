const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bicicletaSchema = new Schema({
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number], index: {type: '2dsphere', sparse: true}
  }
});


bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
  return new this({code, color, modelo, ubicacion});
};

bicicletaSchema.methods.toString = function(){
  return 'code: ' + this.code + ' | color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb){
  return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb){
  //this.create(aBici, cb);
  this.create(aBici, function(err, small){
    if (err) return handleError(err);
  });  
};

bicicletaSchema.statics.findByCode = function(aCode, cb){
  return this.findOne({code: aCode}, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode, cb){
  return this.deleteOne({code: aCode}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);




////antiguo

/*
var Bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
    return 'id: ' + this.id + "| color: " + this.color;
}

Bicicleta.allBicis = []
Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBici}`);
}

Bicicleta.removeById = function(aBiciId) {
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

var a = new Bicicleta(1, 'rojo', 'urbana', [-17.3601735,-66.177221]);
var b = new Bicicleta(2, 'azul', 'urbana', [-17.3601735,-65.177221]);

Bicicleta.add(a);
Bicicleta.add(b);



module.exports = Bicicleta;

*/