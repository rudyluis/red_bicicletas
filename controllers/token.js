var Usuario = require('../models/usuario');
var Usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = {
    confirmationGet: function(req, res, next){
        Token.findOne({token: req.params.token}, function(err, token){
            
            if(token == null) return res.status(400).send({type: 'no-verified', msg:'No se pudo verificar el token'});

            Usuario.findById({_id: token._userId}, function(err, usuario){
                if(usuario == null) return res.status(400).send({type: 'no-verified', msg:'No se pudo verificar el usuario'});                
                if(usuario.verificado) return res.redirect('/usuarios');

                usuario.verificado = true;
                usuario.save(function(err){
                    if(err) return res.status(500).send({msg:err.message});
                    res.redirect('/');
                });
            });
        });
    }
};