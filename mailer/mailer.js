/*const nodemailer = require('nodemailer');

const mailConfig = {
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'olen.veum67@ethereal.email',
        pass: '2P3UF5CuMk4yTAKQNT'
    }
};

module.exports = nodemailer.createTransport(mailConfig);
*/

const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
if(process.env.NODE_ENV ==='production'){
	const options={
		auth:{
			api_key:process.env.SENDGRID_API_SECRET
		}
	}
}else{
	if(process.env.NODE_ENV='staging'){
		console.log('ddddd');
		const options={
			auth:{
				api_key: process.env.SENDGRID_API_SECRET
			}
		}
		mailConfig=sgTransport(options);	
	}
	else{
		const mailConfig = {
			host: 'smtp.ethereal.email',
			port: 587,
			auth: {
				user: process.env.ethereal_user,
				pass: process.env.ethereal_pwd
			}
		};

	}	
}

/*const mailConfig = {
	host: 'smtp.ethereal.email',
	port: 587,
	auth: {
		user: 'olen.veum67@ethereal.email',
		pass: '2P3UF5CuMk4yTAKQNT'
	}
};*/

module.exports = nodemailer.createTransport(mailConfig);
